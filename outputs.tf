output "vpc_id" {
  value = module.vpc.vpc_id
}

output "nlb_id" {
  value = aws_lb.nlb.id
}

output "target_group_arn" {
  value = aws_lb_target_group.tcp_target_group.arn
}