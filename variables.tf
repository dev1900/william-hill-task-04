variable "access_key" {
  type = string
  description = "AWS Access Key credential"
}

variable "secret_key" {
  type = string
  description = "AWS Secret Key credential"
}

variable "region" {
  type = string
  description = "AWS region, where resources will be created"
  default = "eu-west-1"
}

variable "common" {
  description = "Common name for resources"
  type = string
}

variable "env" {
  description = "Environment"
  type = string
}

variable "vpc_azs" {
  description = "VPC availability zones"
  type        = list(string)
}

variable "vpc_cidr" {
  description = "VPC cidr blocks"
  type        = string
}

variable "vpc_private_subnets" {
  description = "VPC private subnets"
  type        = list(string)
}

variable "vpc_public_subnets" {
  description = "VPC public subnets"
  type        = list(string)
}

variable "scale_up_recurrence" {
  description = "Cron schedule expression"
  type        = string
}

variable "scale_down_recurrence" {
  description = "Cron schedule expression"
  type        = string
}

variable "asg_min_size" {
  description = "Minimum size for ASG"
  type        = number
}

variable "asg_max_size" {
  description = "Maximum size for ASG"
  type        = number
}

variable "asg_scale_up_size" {
  description = "Maximum size for ASG"
  type        = number
}

variable "asg_scale_down_size" {
  description = "Maximum size for ASG"
  type        = number
}