module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.6.0"

  name                 = "${var.common}-vpc-${var.env}-${var.region}"
  azs                  = var.vpc_azs
  cidr                 = var.vpc_cidr
  private_subnets      = var.vpc_private_subnets
  public_subnets       = var.vpc_public_subnets
  enable_nat_gateway   = true
  enable_vpn_gateway   = true
  enable_dns_hostnames = true
  enable_dns_support   = true
}

module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name   = "${var.common}-security-group-${var.env}-${var.region}"
  vpc_id = module.vpc.vpc_id
}

resource "aws_launch_template" "template" {
  name_prefix   = "${var.common}-template-${var.env}-${var.region}-"
  image_id      = "ami-01efa4023f0f3a042"
  instance_type = "t2.micro"
}

resource "aws_autoscaling_group" "asg" {
  availability_zones = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
  desired_capacity   = 2
  max_size           = 4
  min_size           = 0

  launch_template {
    id = aws_launch_template.template.id
  }
}

resource "aws_lb" "nlb" {
  internal           = false
  load_balancer_type = "network"
  subnets            = module.vpc.public_subnets
}

resource "aws_lb_target_group" "tcp_target_group" {
  port        = 31555
  protocol    = "TCP"
  target_type = "ip"
  vpc_id      = module.vpc.vpc_id
}

resource "aws_lb_listener" "tcp_listener" {
  load_balancer_arn = aws_lb.nlb.arn
  port              = "80"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tcp_target_group.arn
  }
}

resource "aws_autoscaling_schedule" "scale_up" {
  scheduled_action_name  = "scale_up"
  min_size               = var.asg_min_size
  max_size               = var.asg_max_size
  desired_capacity       = var.asg_scale_up_size
  recurrence             = var.scale_up_recurrence
  autoscaling_group_name = aws_autoscaling_group.asg.name
}

resource "aws_autoscaling_schedule" "scale_down" {
  scheduled_action_name  = "scale_down"
  min_size               = var.asg_min_size
  max_size               = var.asg_max_size
  desired_capacity       = var.asg_scale_down_size
  recurrence             = var.scale_down_recurrence
  autoscaling_group_name = aws_autoscaling_group.asg.name
}