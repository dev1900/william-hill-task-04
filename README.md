# Option 4
## The Task - Infrastructure as code

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

## Usage

This task was implemented using Terraform v1.0.11.

Before initialization and applying code, please provide AWS credentials (`access_key` and `secret_key`) to associated variables. You can do that in `terraform.auto.tfvars`.

```sh
access_key = "<YOUR_ACCESS_KEY>"
secret_key = "<YOUR_SECRET_KEY>"
```

Default region where infrastructure will be created is `eu-west-1`.

Go to root terraform directory, verify/update configuration in terraform.auto.tfvars and run:

```sh
terraform init # Download required Terraform providers and modules
```
```sh
terraform apply # Create or update infrastructure resources or do a new deployment
```

## Design
Code was desiged with our defined internal Terraform standards:
- Splitting tf code into more files, so the code becomes more readeable
- Code has defined some variables to achieve more flexible solution
- Naming conventions and block formatting for better readability

## Implementation
- For managing infrastructure in AWS was used Terraform
- For VPC and Security Group were used official terraform modules